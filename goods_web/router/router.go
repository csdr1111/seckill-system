package router

import (
	"github.com/gin-gonic/gin"
	"goods_web/controller/product"
	"goods_web/controller/seckill"
	"goods_web/controller/user"
)

/*
	总路由
 */
func InitRouter(router *gin.Engine) {
	user_group := router.Group("/user")

	product_group := router.Group("/product")

	seckill_group := router.Group("/seckill")


	// 调用
	user.Router(user_group)
	product.Router(product_group)
	seckill.Router(seckill_group)
}