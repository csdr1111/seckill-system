package user

import (
	"github.com/gin-gonic/gin"
	"goods_web/utils"
	"net/http"
	"strings"
)

func TestGenToken(c *gin.Context) {
	userName := "jack"

	genToken, err := utils.GenToken(userName, utils.FrontUserTokenExpireDuration, utils.FrontUserSecretKey)
	if err != nil {
		c.JSON(http.StatusOK, gin.H{
			"code": 500,
			"msg": "生成token失败",
		})
	}else {
		c.JSON(http.StatusOK, gin.H{
			"code": 200,
			"msg": genToken,
		})
	}
}


func TestValidToken(c *gin.Context) {
	// 获取token
	headerToken := c.Request.Header.Get("Authorization")
	// fmt.Println(headerToken)

	// 切分token
	splitString := strings.Split(headerToken, " ")[1]
	// fmt.Println(splitString)
	// 认证token

	userToken, _ := utils.VerifyToken(splitString, utils.FrontUserSecretKey)

	c.JSON(http.StatusOK, gin.H{
		"code": 200,
		"msg": userToken,
	})
}