package main

import (
	"goods_web/rabbitmq"
	"goods_web/utils"
)

func main() {
	configMQ := rabbitmq.QueueAndExchange{
		QueueName:    "orderQueue",
		ExchangeName: "orderExchange",
		ExchangeType: "direct",
		ExchangeKey:  "orderKey",
	}

	mq := rabbitmq.NewRabbitMQ(configMQ)
	mq.ConnMQ()
	mq.CreateChannel()
	defer mq.CloseMQ()
	defer mq.CloseChannel()

	data := map[string]interface{}{
		"email": "123",
		"id": 6,
	}

	mq.PublishMsg(utils.MapToString(data))

}