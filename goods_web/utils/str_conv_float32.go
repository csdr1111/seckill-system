package utils

import (
	"strconv"
)

func StrToFloat32(str string) float32  {

	valueFloat64,_ := strconv.ParseFloat(str,64)  // 字符串转64位
	return float32(valueFloat64)
}
