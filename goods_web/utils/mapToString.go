package utils

import (
	"encoding/json"
)

// map转string
func MapToString(m map[string]interface{}) string  {
	bytes, _ := json.Marshal(m)
	return string(bytes)
}

// string转map
func StringToMap(s string) map[string]interface{} {
	var res map[string]interface{}
	err := json.Unmarshal([]byte(s), &res)
	if err != nil {
		panic(err)
	}

	return res
}


