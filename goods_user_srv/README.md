# Goods_user_srv Service

This is the Goods_user_srv service

Generated with

```
micro new --namespace=go.micro --type=service goods_user_srv
```

## Getting Started

- [Configuration](#configuration)
- [Dependencies](#dependencies)
- [Usage](#usage)

## Configuration

- FQDN: go.micro.service.goods_user_srv
- Type: service
- Alias: goods_user_srv

## Dependencies

Micro services depend on service discovery. The default is multicast DNS, a zeroconf system.

In the event you need a resilient multi-host setup we recommend etcd.

```
# install etcd
brew install etcd

# run etcd
etcd
```

## Usage

A Makefile is included for convenience

Build the binary

```
make build
```

Run the service
```
./goods_user_srv-service
```

Build a docker image
```
make docker
```