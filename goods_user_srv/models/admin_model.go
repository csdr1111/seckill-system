package models


import "time"

/*
用户模型类
*/
type AdminUser struct {
	Id int `gorm:"type:int(11)"`
	UserName string `gorm:"type:varchar(64)"`
	Password string `gorm:"type:varchar(64)"`
	Desc string `gorm:"type:varchar(255)"`
	Status int `gorm:"type:int(2)"`
	CreateTime time.Time
}

// 指定表名
func (AdminUser) TableName() string {
	return "admin_user"
}