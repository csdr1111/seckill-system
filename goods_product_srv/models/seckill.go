package models

import "time"

// 秒杀服务模型类
type SecKills struct {
	Id int
	Name string
	Price float32 `gorm:"type:decimal(11,2)"`
	Num int
	PId int
	Status int `gorm:"default:0"`  // 0表示未下架 1表示已下架
	StartTime time.Time
	EndTime time.Time
	CreateTime time.Time
}

func (SecKills)TableName() string {
	return "seckills"
}