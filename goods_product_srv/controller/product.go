package controller

import (
	"context"
	"errors"
	"goods_product_srv/data_source"
	"goods_product_srv/models"
	product "goods_product_srv/proto/product"
	"time"
)

type Products struct {

}

// 商品列表
func (p *Products)ProductsList(ctx context.Context, in *product.ProductsRequest, out *product.ProductsResponse) error {
	// 接收参数
	currentPage := in.CurrentPage
	pageSize := in.PageSize

	// 查询总页数
	var total int32
	productsTotal := []models.Product{}
	data_source.Db.Find(&productsTotal).Count(&total)

	// 查询数据
	products := []models.Product{}
	offectNum := (currentPage - 1) * pageSize
	find := data_source.Db.Limit(pageSize).Offset(offectNum).Find(&products)

	// 需要返回的数据
	productsResponse := []*product.Product{}
	for _, value := range products {
		temp := &product.Product{}
		temp.Id = int32(value.Id)
		temp.Name = value.Name
		temp.Price = value.Price
		temp.Num = int32(value.Num)
		temp.Uint = value.Uint
		temp.Pic = value.Pic
		temp.Desc = value.Desc
		temp.CreateTime = value.CreateTime.Format("2006-01-02 15:04:05")
		productsResponse = append(productsResponse, temp)
	}

	if find.Error != nil {
		out.Msg = "服务器错误"
		out.Code = 500
		return errors.New("服务器错误")
	}else {
		out.Code = 200
		out.Msg = "查询数据成功"
		out.Products = productsResponse
		out.PageSize = pageSize
		out.Current = currentPage
		out.Total = total
	}

	return nil
}

// 添加商品
func (p *Products)AddProduct(ctx context.Context, in *product.AddProductRequest, out *product.AddProductResponse) error {
	// 接收参数
	name := in.Name
	price := in.Price
	num := in.Num
	uint := in.Uint
	pic := in.Pic
	desc := in.Desc
	// 保存到数据库
	product := models.Product{
		Name:       name,
		Price:      price,
		Num:        int(num),
		Uint:       uint,
		Pic:        pic,
		Desc:       desc,
		CreateTime: time.Now(),
	}
	db := data_source.Db.Create(&product)
	if db.Error != nil {
		out.Code = 500
		out.Msg = "添加商品失败"
	}else {
		out.Code = 200
		out.Msg = "添加商品成功"
	}
	return nil
}

// 删除商品
func (p *Products) DeleteProduct(ctx context.Context, in *product.DeleteProductRequest, out *product.DeleteProductResponse) error {
	// 接收参数
	id := in.Id
	// 删除数据
	goods := models.Product{}
	result := data_source.Db.Where("id = ?", id).Delete(&goods)
	if result.Error != nil {
		out.Code = 500
		out.Msg = "删除数据失败"
	}else {
		out.Code = 200
		out.Msg = "删除数据成功"
	}
	return nil
}

// 进入修改商品界面
func (p *Products)ToEditProduct(ctx context.Context, in *product.ToEditProductRequest, out *product.ToEditProductResponse) error {
	// 接收参数
	id := in.Id
	// 查询数据
	products := models.Product{}
	result := data_source.Db.Where("id = ?", id).First(&products)

	if result.Error != nil {
		out.Code = 500
		out.Msg = "商品id有误"
		return errors.New("商品id有误")
	}else {
		// 转换数据
		resultProduct := &product.Product{
			Id:         int32(products.Id),
			Name:       products.Name,
			Price:      products.Price,
			Num:        int32(products.Num),
			Uint:       products.Uint,
			Pic:        products.Pic,
			Desc:       products.Desc,
		}


		out.Code = 200
		out.Msg = "查询成功"
		out.Product = resultProduct
		return nil
	}
}

// 修改商品
func (p *Products)DoEditProduct(ctx context.Context, in *product.DoEditProductRequest, out *product.DoEditProductResponse) error  {
	id := in.Id
	name := in.Name
	price := in.Price
	num := in.Num
	uint := in.Uint
	pic := in.Pic
	desc := in.Desc
	// 保存到数据库
	product := models.Product{
		Id:         int(id),
		Name:       name,
		Price:      price,
		Num:        int(num),
		Uint:       uint,
		Pic:        pic,
		Desc:       desc,
		CreateTime: time.Now(),
	}
	db := data_source.Db.Model(&product).Updates(product)
	if db.Error != nil {
		out.Code = 500
		out.Msg = "更新商品失败"
	}else {
		out.Code = 200
		out.Msg = "更新商品成功"
	}
	return nil
}