// Code generated by protoc-gen-micro. DO NOT EDIT.
// source: product.proto

package go_micro_service_goods_product_srv

import (
	fmt "fmt"
	proto "github.com/golang/protobuf/proto"
	math "math"
)

import (
	context "context"
	api "github.com/micro/go-micro/v2/api"
	client "github.com/micro/go-micro/v2/client"
	server "github.com/micro/go-micro/v2/server"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion3 // please upgrade the proto package

// Reference imports to suppress errors if they are not otherwise used.
var _ api.Endpoint
var _ context.Context
var _ client.Option
var _ server.Option

// Api Endpoints for Products service

func NewProductsEndpoints() []*api.Endpoint {
	return []*api.Endpoint{}
}

// Client API for Products service

type ProductsService interface {
	ProductsList(ctx context.Context, in *ProductsRequest, opts ...client.CallOption) (*ProductsResponse, error)
	AddProduct(ctx context.Context, in *AddProductRequest, opts ...client.CallOption) (*AddProductResponse, error)
	DeleteProduct(ctx context.Context, in *DeleteProductRequest, opts ...client.CallOption) (*DeleteProductResponse, error)
	ToEditProduct(ctx context.Context, in *ToEditProductRequest, opts ...client.CallOption) (*ToEditProductResponse, error)
	DoEditProduct(ctx context.Context, in *DoEditProductRequest, opts ...client.CallOption) (*DoEditProductResponse, error)
}

type productsService struct {
	c    client.Client
	name string
}

func NewProductsService(name string, c client.Client) ProductsService {
	return &productsService{
		c:    c,
		name: name,
	}
}

func (c *productsService) ProductsList(ctx context.Context, in *ProductsRequest, opts ...client.CallOption) (*ProductsResponse, error) {
	req := c.c.NewRequest(c.name, "Products.ProductsList", in)
	out := new(ProductsResponse)
	err := c.c.Call(ctx, req, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *productsService) AddProduct(ctx context.Context, in *AddProductRequest, opts ...client.CallOption) (*AddProductResponse, error) {
	req := c.c.NewRequest(c.name, "Products.AddProduct", in)
	out := new(AddProductResponse)
	err := c.c.Call(ctx, req, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *productsService) DeleteProduct(ctx context.Context, in *DeleteProductRequest, opts ...client.CallOption) (*DeleteProductResponse, error) {
	req := c.c.NewRequest(c.name, "Products.DeleteProduct", in)
	out := new(DeleteProductResponse)
	err := c.c.Call(ctx, req, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *productsService) ToEditProduct(ctx context.Context, in *ToEditProductRequest, opts ...client.CallOption) (*ToEditProductResponse, error) {
	req := c.c.NewRequest(c.name, "Products.ToEditProduct", in)
	out := new(ToEditProductResponse)
	err := c.c.Call(ctx, req, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *productsService) DoEditProduct(ctx context.Context, in *DoEditProductRequest, opts ...client.CallOption) (*DoEditProductResponse, error) {
	req := c.c.NewRequest(c.name, "Products.DoEditProduct", in)
	out := new(DoEditProductResponse)
	err := c.c.Call(ctx, req, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// Server API for Products service

type ProductsHandler interface {
	ProductsList(context.Context, *ProductsRequest, *ProductsResponse) error
	AddProduct(context.Context, *AddProductRequest, *AddProductResponse) error
	DeleteProduct(context.Context, *DeleteProductRequest, *DeleteProductResponse) error
	ToEditProduct(context.Context, *ToEditProductRequest, *ToEditProductResponse) error
	DoEditProduct(context.Context, *DoEditProductRequest, *DoEditProductResponse) error
}

func RegisterProductsHandler(s server.Server, hdlr ProductsHandler, opts ...server.HandlerOption) error {
	type products interface {
		ProductsList(ctx context.Context, in *ProductsRequest, out *ProductsResponse) error
		AddProduct(ctx context.Context, in *AddProductRequest, out *AddProductResponse) error
		DeleteProduct(ctx context.Context, in *DeleteProductRequest, out *DeleteProductResponse) error
		ToEditProduct(ctx context.Context, in *ToEditProductRequest, out *ToEditProductResponse) error
		DoEditProduct(ctx context.Context, in *DoEditProductRequest, out *DoEditProductResponse) error
	}
	type Products struct {
		products
	}
	h := &productsHandler{hdlr}
	return s.Handle(s.NewHandler(&Products{h}, opts...))
}

type productsHandler struct {
	ProductsHandler
}

func (h *productsHandler) ProductsList(ctx context.Context, in *ProductsRequest, out *ProductsResponse) error {
	return h.ProductsHandler.ProductsList(ctx, in, out)
}

func (h *productsHandler) AddProduct(ctx context.Context, in *AddProductRequest, out *AddProductResponse) error {
	return h.ProductsHandler.AddProduct(ctx, in, out)
}

func (h *productsHandler) DeleteProduct(ctx context.Context, in *DeleteProductRequest, out *DeleteProductResponse) error {
	return h.ProductsHandler.DeleteProduct(ctx, in, out)
}

func (h *productsHandler) ToEditProduct(ctx context.Context, in *ToEditProductRequest, out *ToEditProductResponse) error {
	return h.ProductsHandler.ToEditProduct(ctx, in, out)
}

func (h *productsHandler) DoEditProduct(ctx context.Context, in *DoEditProductRequest, out *DoEditProductResponse) error {
	return h.ProductsHandler.DoEditProduct(ctx, in, out)
}
