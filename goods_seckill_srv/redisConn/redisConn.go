package redisConn

import (
	"fmt"
	// "github.com/garyburd/redigo/redis"
	"github.com/go-redis/redis"
)

var (
	RedisDb *redis.Client
)

func init() {
	RedisDb = redis.NewClient(&redis.Options{
		Addr:     "localhost:6379",
		Password: "",  // no password set
		DB:       0,   // use default DB
		PoolSize: 100, // 连接池大小
	})

	_, err := RedisDb.Ping().Result()
	fmt.Println(err)
}