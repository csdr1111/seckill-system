package models

import "time"

// 秒杀服务模型类
type SecKills struct {
	Id         int
	Name       string
	Price      float32 `gorm:"type:decimal(11,2)"`
	Num        int
	PId        int
	Status     int `gorm:"default:0"` // 0表示未下架 1表示已下架
	StartTime  time.Time
	EndTime    time.Time
	CreateTime time.Time
	Orders     []Orders `gorm:"foreignKey:Sid;references:Id"`
}

func (SecKills) TableName() string {
	return "seckills"
}

// 订单模型类
type Orders struct {
	Id        int
	UserEmail string // 用户
	// 一个订单只能属于一个活动， 一个活动有多个订单
	Sid int

	CreateTime time.Time

	// 支付状态 0表示未支付 1表示已支付
	PayStatus int `gorm:"default:0"`
}


func (Orders) TableName() string {
	return "orders"
}