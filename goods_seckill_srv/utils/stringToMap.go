package utils

import (
	"encoding/json"
	"fmt"
	"strconv"
)


/*
反序列化之后的int类型会变为float64
 */
// string转map
func StringToMap(s []byte) map[string]interface{} {
	var res map[string]interface{}
	err := json.Unmarshal(s, &res)   // res中的id是float64类型

	id, _ := strconv.Atoi(res["id"].(string))

	data := map[string]interface{}{
		"email": res["email"],
		"id": id,
	}

	fmt.Printf("%v %T %v %T\n", data["email"],data["email"], data["id"], data["id"])

	if err != nil {
		panic(err)
	}

	//var data map[string]interface{}
	//decoder := json.NewDecoder(bytes.NewReader([]byte(s)))
	//decoder.UseNumber()
	//err := decoder.Decode(&data)
	//if err != nil {
	//	fmt.Printf("unmarshal failed, err:%v\n", err)
	//}
	//fmt.Printf("%v %T %v %T\n", data["email"],data["email"], data["id"], data["id"])
	//
	//// 将m2["count"]转为json.Number之后调用Int64()方法获得int64类型的值
	//id, err := data["id"].(json.Number).Int64()
	//if err != nil {
	//	fmt.Printf("parse to int64 failed, err:%v\n", err)
	//}
	//// fmt.Printf("type:%T\n", int(id)) // int
	//
	//res := map[string]interface{}{
	//	"email": data["email"],
	//	"id": int(id),
	//}


	return data
}

// map转string
func MapToString(m map[string]interface{}) string  {
	bytes, _ := json.Marshal(m)
	return string(bytes)
}