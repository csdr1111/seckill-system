package main

import (
	"github.com/micro/go-micro/v2"
	log "github.com/micro/go-micro/v2/logger"
	"github.com/micro/go-micro/v2/registry"
	"github.com/micro/go-micro/v2/registry/etcd"
	"goods_seckill_srv/controller"
	seckill "goods_seckill_srv/proto/seckill"
)

func main() {
	// New Service
	service := micro.NewService(
		micro.Name("go.micro.service.goods_seckill_srv"),
		micro.Version("latest"),
		micro.Registry(etcd.NewRegistry(
			registry.Addrs("127.0.0.1:2379"),
			)),
	)

	// Initialise service
	service.Init()

	// Register Handler
	seckill.RegisterSeckillHandler(service.Server(), new(controller.Seckill))

	// Run service
	if err := service.Run(); err != nil {
		log.Fatal(err)
	}
}
