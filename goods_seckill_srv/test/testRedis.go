package main

import (
	"fmt"
	"goods_seckill_srv/redisConn"
	"time"
)

func main() {
	//c, err := redis.Dial("tcp", "127.0.0.1:6379")
	//if err != nil {
	//	fmt.Println("Connect to redis error", err)
	//	return
	//}
	//defer c.Close()
	//
	//_, err = c.Do("SET", "mykey", "superWang", "EX", "10")
	//if err != nil {
	//	fmt.Println("redis set failed:", err)
	//}
	//
	//username, err := redis.String(c.Do("GET", "mykey"))
	//if err != nil {
	//	fmt.Println("redis get failed:", err)
	//} else {
	//	fmt.Printf("Get mykey: %v \n", username)
	//}
	//
	//time.Sleep(8 * time.Second)
	//
	//username, err = redis.String(c.Do("GET", "mykey"))
	//if err != nil {
	//	fmt.Println("redis get failed:", err)
	//} else {
	//	fmt.Printf("Get mykey: %v \n", username)
	//}

	s, err := redisConn.RedisDb.Set("key", 1, 100*time.Second).Result()
	fmt.Println(s, err)

	result, errr := redisConn.RedisDb.Get("key").Result()
	fmt.Println(result, errr)
	if err != nil {
		fmt.Println("没有")
	}else {
		fmt.Printf("%v  %T", result, result)  // value  string
	}


}
